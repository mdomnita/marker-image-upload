import * as express from 'express'
import * as multer from 'multer'
import * as cors from 'cors'
import * as fs from 'fs'
import * as path from 'path'
import * as bodyParser from 'body-parser'
import { imageFilter, loadCollection } from './utils';

// setup
const COLLECTION_NAME = 'images'
const UPLOAD_PATH = 'uploads';
const GEOJSON_PATH = path.join(__dirname, '../static/data/proc/markersReg.geojson')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${UPLOAD_PATH}/`)
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}.jpg`)
    }
})
const upload = multer({ storage: storage, fileFilter: imageFilter });

// optional: clean all data before start
// cleanFolder(UPLOAD_PATH);

// app
const app = express();
app.use(cors());
app.use(express.static('static'))
app.use(express.static('uploads'))
// app.use(express.static(`${UPLOAD_PATH}`))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get('/', async (req, res) => {
    // default route
    res.send('index.html');
})

app.post('/marker/upload', upload.any(), async (req, res) => {
    try {
        let obj = {}
        let fullUrl = `${req.protocol}://${req.hostname}:${req.connection.localPort}`
        for (let key in req.files) {
            let datum = req.files[key]
            obj[datum.fieldname] = `${fullUrl}/${datum.filename}`
        }
        res.send(obj)
    } catch (err) {
        console.log(err)
        res.status(400).send('Bad Request')
    }
})

app.get('/exists/:imageName', async (req, res) => {
    let imagePath = path.join(__dirname, `../${UPLOAD_PATH}/${req.params.imageName}`)
    fs.stat(imagePath, async (err, stats) => {
        res.json({exists: !err ? stats && stats.isFile(): false})
    })
})

app.post('/marker/:id', async (req, res) => {
    try {
        let markerId = req.params.id
        console.log(markerId)
        const body = req.body
        const {nom, categorie, adresse, description, ouvert, pub, lat, long} = req.body
        console.log(body) 

        let rawData = fs.readFileSync(GEOJSON_PATH)
        let data = JSON.parse(rawData.toString())
        let isUpdated: boolean = false

        for (let key in data.features) {
            if (data.features[key].properties.id != markerId) continue
            data.features[key].properties = Object.assign(
                {},
                data.features[key].properties,
                {
                    'NOM': body['NOM'] || '',
                    'CATEGORIE':body['CATEGORIE'] || '',
                    'ADRESSE': body['ADRESSE'] || '',
                    'Description': body['Description'] || '',
                    'OUVERT': body['OUVERT'] || '',
                    'Public': body['Public'] || '',
                    'Lat': body['Lat'] || '',
                    'Long': body['Long'] || ''
                })
            isUpdated = true

        }
        if (isUpdated){
            fs.writeFileSync(GEOJSON_PATH, JSON.stringify(data))
            res.status(200).json({success: true})
        }
        else res.status(400).json({error: `can not find the marker_id: ${markerId}`})

    } catch (err) {
        console.log(err)
        res.status(500).json({error: err.stack})
    }
})

app.use(function (err, req, res, next) {
    if (res.headersSent) {
        return next(err)
    }
    if (err.stack.indexOf('Only image files are allowed') >= 0)
        res.status(400).json({error: err.stack})
    else res.status(500).json({error: err.stack})
})

app.listen(3000, function () {
    console.log('listening on port 3000!');
})